# Copyright 2014-2025 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=SSSD release=${PV} suffix=tar.gz ] \
    pam \
    systemd-service \
    udev-rules \
    autotools [ supported_autoconf=[ 2.7 ] supported_automake=[ 1.17 ] ]

SUMMARY="System Security Services Daemon"
DESCRIPTION="
Provides a set of daemons to manage access to remote directories and authentication mechanisms. It
provides an NSS and PAM interface toward the system and a pluggable backend system to connect to
multiple different account sources. It is also the basis to provide client auditing and policy
services for projects like FreeIPA.
"

UPSTREAM_RELEASE_NOTES="https://docs.pagure.org/SSSD.${PN}/users/relnotes/notes_${PV//./_}.html [[ lang = en ]]"

LICENCES="GPL-3"
SLOT="0"
PLATFORMS="~amd64 ~armv7 ~armv8 ~x86"
MYOPTIONS="
    acl
    nfsv4
    systemd
    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
    ( linguas: bg ca cs de es eu fi fr hu id it ja ka ko nb nl pl pt pt_BR ru sv tg tr uk zh_CN zh_TW )
"

# auth-tests are slooow
RESTRICT="test"

DEPENDENCIES="
    build:
        app-text/docbook-xml-dtd:4.4
        dev-libs/libxslt
        sys-devel/gettext[>=0.14.4]
        virtual/pkg-config[>=0.9.0]
    build+run:
        group/sssd
        user/sssd
        app-crypt/krb5[>=1.20] [[ note = [ won't work with heimdal (yet) ] ]]
        dev-db/tdb[>=1.1.3]
        net-libs/cyrus-sasl[kerberos]
        dev-libs/ding-libs[>=0.6.0]
        dev-libs/jansson
        dev-libs/libunistring:=
        dev-libs/p11-kit:1
        dev-libs/pcre2
        dev-libs/popt
        dev-libs/talloc
        dev-libs/tevent[>=0.11.0]
        net-directory/openldap[>=2.4]
        net-dns/bind-tools[kerberos]
        net-dns/c-ares
        net-fs/samba[>=4.21.0] [[ note = [ for ldb ] ]]
        net-libs/libnl:3.0[>=3.0]
        sys-apps/dbus
        sys-apps/keyutils
        sys-libs/libcap
        sys-libs/pam
        sys-apps/util-linux [[ note = [ for libuuid ] ]]
        acl? ( net-fs/cifs-utils[ads] )
        nfsv4? ( net-fs/nfs-utils[nfsv4] )
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl:= )
        systemd? ( sys-apps/systemd [[ note = [ for journald and logind ] ]] )
"
#    test-expensive:
#        dev-libs/check[>=0.9.5]
#        dev-util/cmocka
#        sys-libs/nss_wrapper
#        sys-libs/pam_wrapper
#        sys-libs/uid_wrapper

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/${PN}-2.1.0-nsupdate-realm-check.patch
)

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --localstatedir=/var
    --enable-gss-spnego-for-zero-maxssf
    --enable-kcm-renewal
    --enable-krb5-locator-plugin
    --enable-nls
    --enable-pammoddir=$(getpam_mod_dir)
    --enable-polkit-rules-path=/usr/share/polkit-1/rules.d
    --enable-sss-default-nss-plugin
    --disable-ldb-version-check
    --disable-static
    --disable-systemtap
    --disable-rpath
    --disable-valgrind
    --with-ad-gpo-default=enforcing
    --with-autofs
    --with-cifs-plugin-path=/usr/$(exhost --target)/lib/cifs-utils
    --with-db-path=/var/lib/sss/db
    --with-environment-file=/etc/conf.d/sssd.conf
    --with-kcm
    --with-ldb-lib-dir=/usr/$(exhost --target)/lib/samba/ldb
    --with-libnl
    --with-log-path=/var/log/sssd
    --with-manpages
    --with-mcache-path=/var/lib/sss/mc
    --with-pid-path=/run/sssd/
    --with-pipe-path=/var/lib/sss/pipes
    --with-plugin-path=/usr/$(exhost --target)/lib/sssd
    --with-pubconf-path=/var/lib/sss/pubconf
    --with-secrets-db-path=/var/lib/sss/secrets
    --with-sssd-user=sssd
    --with-ssh
    --with-sudo
    --with-systemd-sysusersdir=/usr/$(exhost --target)/lib/sysusers.d
    --with-systemdunitdir=${SYSTEMDSYSTEMUNITDIR}
    --with-tmpfilesdir=${SYSTEMDTMPFILESDIR}
    --with-udevrulesdir=${UDEVRULESDIR}
    --with-winbind-plugin-path=/usr/$(exhost --target)/lib/samba/idmap
    --without-allow-remote-domain-local-groups
    --without-conf-service-user-support
    --without-extended-enumeration-support
    # set to /usr/$(exhost --target)/bin/ipa-getkeytab
    --without-ipa-getkeytab
    --without-oidc-child
    --without-passkey
    --without-python2-bindings
    --without-python3-bindings
    # TODO: add samba now that it's possible to build with krb5
    --without-samba
    --without-selinux
    --without-ssh-known-hosts-proxy
    --without-subid
)

DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    "acl cifs-idmap-plugin"
)
DEFAULT_SRC_CONFIGURE_OPTION_WITHS=(
    "nfsv4 nfsv4-idmapd-plugin"
)

DEFAULT_SRC_CONFIGURE_OPTIONS=(
    "systemd --with-initscript=systemd"
    "systemd --with-syslog=journald --with-syslog=syslog"
)

src_install() {
    default

    # environment configuration file
    insinto /etc/conf.d
    hereins sssd.conf <<EOF
SSSD_OPTIONS=""
EOF

    # install a sane default configuration
    insinto /etc/${PN}
hereins sssd.conf <<EOF
[sssd]
services = nss, pam

[nss]

[pam]
EOF

    # remove config file installed to the wrong location
    edo rm "${IMAGE}"/usr/$(exhost --target)/lib/sssd/conf/sssd.conf

    # kerberos files
    insinto /etc/krb5.conf.d
    doins contrib/{enable_sssd_conf_dir,kcm_default_ccache}

    # remove *.la files to fix e.g. /usr/lib64/ldb/memberof.la: invalid ELF header
    find "${IMAGE}" -name "*.la" -exec rm -f {} \;

    # remove analyzer tools and python stuff which are installed unconditionally but
    # depend on python, last checked: 2.6.3
    edo rm "${IMAGE}"/usr/$(exhost --target)/libexec/sssd/sss_analyze
    edo rm -rf "${IMAGE}"/sssd

    # remove multiple empty directories
    edo find "${IMAGE}" -type d -empty -delete

    # set permissions (see contrib/sssd.spec.in)
    edo chown -R root:sssd "${IMAGE}"/etc/sssd
    edo chmod -R g+r "${IMAGE}"/etc/sssd

    edo chown root:sssd "${IMAGE}"/usr/$(exhost --target)/libexec/sssd/sssd_pam
    edo chmod 0750 "${IMAGE}"/usr/$(exhost --target)/libexec/sssd/sssd_pam

    keepdir /var/lib/sss/{db,deskprofile,gpo_cache,keytabs,mc,pipes{,/private},pubconf{,/krb5.include.d},secrets}
    edo chown sssd:sssd "${IMAGE}"/var/lib/sss/{db,deskprofile,gpo_cache,keytabs,mc,pipes{,/private},pubconf{,/krb5.include.d},secrets}
    edo chmod 0775 "${IMAGE}"/var/lib/sss
    edo chmod 0770 "${IMAGE}"/var/lib/sss/db
    edo chmod 0775 "${IMAGE}"/var/lib/sss/mc
    edo chmod 0770 "${IMAGE}"/var/lib/sss/secrets
    edo chmod 0771 "${IMAGE}"/var/lib/sss/deskprofile
    edo chmod 0775 "${IMAGE}"/var/lib/sss/pipes
    edo chmod 0770 "${IMAGE}"/var/lib/sss/pipes/private
    edo chmod 0775 "${IMAGE}"/var/lib/sss/pubconf
    edo chmod 0770 "${IMAGE}"/var/lib/sss/gpo_cache
    edo chmod 0775 "${IMAGE}"/var/lib/sss/pubconf/krb5.include.d
    edo chmod 0770 "${IMAGE}"/var/lib/sss/keytabs

    keepdir /var/log/sssd
    edo chown sssd:sssd "${IMAGE}"/var/log/sssd
    edo chmod 0770 "${IMAGE}"/var/log/sssd

    keepdir /etc/sssd/{conf.d,pki}
    edo chown root:sssd "${IMAGE}"/etc/sssd{,/conf.d,/pki}
    edo chmod 0750 "${IMAGE}"/etc/sssd{,/conf.d,/pki}

    edo chown root:sssd "${IMAGE}"/etc/sssd/sssd.conf
    edo chmod 0640 "${IMAGE}"/etc/sssd/sssd.conf

    # exclude selinux_child which we don't build
    edo chown root:sssd "${IMAGE}"/usr/$(exhost --target)/libexec/sssd/{krb5,ldap,proxy}_child
    edo chmod 0750 "${IMAGE}"/usr/$(exhost --target)/libexec/sssd/{krb5,ldap,proxy}_child
}

pkg_postinst() {
    # doesn't carry over from src_install so we need to set this here
    edo setcap cap_dac_read_search=p /usr/$(exhost --target)/libexec/sssd/sssd_pam
    edo setcap cap_dac_read_search,cap_setuid,cap_setgid=p /usr/$(exhost --target)/libexec/sssd/krb5_child
    edo setcap cap_dac_read_search=p /usr/$(exhost --target)/libexec/sssd/ldap_child
}

#src_test_expensive() {
#    esandbox allow_net --connect "LOCAL@53"
#    esandbox allow_net "unix:/tmp/sssd-dbus-tests.*/sbus"
#
#    emake check
#
#    esandbox disallow_net "unix:/tmp/sssd-dbus-tests.*/sbus"
#    esandbox disallow_net --connect "LOCAL@53"
#}

