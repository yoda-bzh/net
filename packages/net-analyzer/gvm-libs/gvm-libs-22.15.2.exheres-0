# Copyright 2017-2025 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=greenbone tag=v${PV} ] \
    cmake

SUMMARY="Greenbone Vulnerability Management Libraries"
HOMEPAGE+=" https://www.openvas.org"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    ldap
    ( libc: musl )
"

# requires unpackaged cgreen, last checked: 22.10.0
RESTRICT="test"

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        app-crypt/gpgme[>=1.7.0]
        dev-libs/cjson[>=1.7.14]
        dev-libs/glib:2[>=2.42]
        dev-libs/gnutls[>=3.2.15]
        dev-libs/hiredis[>=0.10.1]
        dev-libs/libgcrypt[>=1.6]
        dev-libs/libgpg-error
        dev-libs/libpcap
        dev-libs/libxml2:2.0[>=2.0]
        net-libs/libnet[>=1.1.2.1]
        net-libs/libssh[>=0.6.0]
        net-misc/curl[>=7.83.0]
        net-libs/paho-mqtt-c[>=1.3.0]
        sys-apps/util-linux[>=2.25.0]
        sys-libs/zlib[>=1.2.8]
        !libc:musl? ( dev-libs/libxcrypt:= )
        ldap? ( net-directory/openldap )
"

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DBUILD_SHARED:BOOL=TRUE
    -DBUILD_STATIC:BOOL=FALSE
    -DBUILD_WITH_RADIUS:BOOL=FALSE
    -DCLANG_FORMAT:BOOL=FALSE
    -DCMAKE_BUILD_TYPE:STRING=Release
    -DCMAKE_DISABLE_FIND_PACKAGE_Git:BOOL=TRUE
    -DDATADIR:PATH=/usr/share
    -DDOXYGEN_EXECUTABLE:BOOL=FALSE
    -DENABLE_COVERAGE:BOOL=FALSE
    -DLIBDIR:PATH=/usr/$(exhost --target)/lib
    -DOPENVASD:BOOL=TRUE
    -DREDIS_SOCKET_PATH:PATH=/tmp/redis.sock
)
CMAKE_SRC_CONFIGURE_OPTIONS=(
    'ldap BUILD_WITH_LDAP'
)
CMAKE_SRC_CONFIGURE_TESTS=(
    '-DBUILD_TESTS:BOOL=TRUE -DBUILD_TESTS:BOOL=FALSE'
)

src_install() {
    cmake_src_install

    # remove empty directory
    edo rm -rf "${IMAGE}"/run
}

