# Copyright 2010-2017 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

SUMMARY="Eggdrop is an extensible IRC bot"
DESCRIPTION="
Eggdrop is a popular IRC bot and is the oldest IRC bot still in active development.
It is written in C and features interfaces for C modules and Tcl scripts that allow
users to further enhance the functionality of the bot.
"
BASE_URI="eggheads.org"
HOMEPAGE="http://www.${BASE_URI} https://github.com/eggheads/${PN}"
DOWNLOADS="
    http://ftp.${BASE_URI}/pub/${PN}/source/$(ever range 1-2)/${PN}-${PV}.tar.gz
    gseen? ( ftp://ftp.${BASE_URI}/pub/${PN}/modules/$(ever range 1-2)/gseen.mod.1.1.1-pseudo.tar.gz )
"

REMOTE_IDS="freecode:${PN}"
UPSTREAM_CHANGELOG="ftp://ftp.${BASE_URI}/pub/${PN}/UPDATES/$(ever range 1-2)/UPDATES${PV}"

PLATFORMS="~amd64 ~x86"
LICENCES="GPL-2"
SLOT="0"
MYOPTIONS="
    gseen [[ description = [ Adds the 3rd-party gseen module. ] ]]
"

DEPENDENCIES="
    build+run:
        dev-lang/tcl
        dev-libs/openssl:=
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --hates=docdir
    --enable-ipv6
    --enable-tls
    --disable-strip
    --with-handlen=16
)
DEFAULT_SRC_INSTALL_PARAMS=( DEST="${IMAGE}"/opt/eggdrop )

pkg_setup() {
    exdirectory --allow /opt
}

src_prepare() {
    default

    option gseen && edo mv "${WORKBASE}"/gseen.mod "${WORK}"/src/mod
}

src_compile() {
    emake -j1 config

    default
}

src_install() {
    dobin "${FILES}"/eggdrop-installer
    doman doc/man1/eggdrop.1
    dodoc doc/WEIRD-MESSAGES

    default

    option gseen && edo cp "${WORK}"/src/mod/gseen.mod/*.lang "${IMAGE}"/opt/eggdrop/language

    edo mv "${IMAGE}"/opt/eggdrop/doc/* "${IMAGE}"/usr/share/doc/${PNVR}
    edo rmdir "${IMAGE}"/opt/eggdrop/doc
    edo rm -r "${IMAGE}"/usr/share/doc/${PNVR}/man1
    edo rm "${IMAGE}"/opt/eggdrop/README
    edo mv "${IMAGE}"/usr/share/doc/${PNVR}/notes.so "${IMAGE}"/opt/eggdrop/modules-${PV}

    keepdir /opt/eggdrop/filesys/incoming
}

