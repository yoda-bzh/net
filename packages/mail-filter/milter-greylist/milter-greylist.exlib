# Copyright 2009-2017 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.15 1.10 ] ] systemd-service

export_exlib_phases src_prepare src_install

SUMMARY="milter-greylist is a stand-alone milter that implements greylist filtering"
DESCRIPTION="
milter-greylist is a stand-alone milter written in C that implements the greylist
filtering method, as proposed by Evan Harris. Grey listing works by assuming that,
unlike legitimate MTA, spam engines will not retry sending their junk mail on a
temporary error. The filter will always reject mail temporarily on a first attempt,
then accept it after some time has elapsed. If spammers ever try to resend rejected
messages, we can assume they will not stay idle between the two sends (if they do,
the spam problem would just be solved). Odds are good that the spammer will send
a mail to a honey pot address and get blacklisted in several real-time distributed
black lists before the second attempt.
"
HOMEPAGE="http://hcpnet.free.fr/${PN}/"
DOWNLOADS="ftp://ftp.espci.fr/pub/${PN}/${PNV}.tgz"

UPSTREAM_CHANGELOG="http://${PN}.wikidot.com/dev-changelog [[ lang = en ]]"
UPSTREAM_DOCUMENTATION="http://${PN}.wikidot.com/generic-notes [[ lang = en ]]"

LICENCES="BSD-3"
SLOT="0"
MYOPTIONS="spamassassin [[ description = [ Support SpamAssassin, e. g. spf and dkim checks ] ]]"

DEPENDENCIES="
    build+run:
        mail-mta/sendmail
        spamassassin? ( mail-filter/spamassassin )
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --disable-drac
    --disable-p0f
    --disable-postfix
    --with-conffile="/etc/mail/greylist.conf"
    --with-dumpfile="/var/lib/milter-greylist"
    --with-user="mail"
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=( spamassassin )
DEFAULT_SRC_COMPILE_PARAMS=( -j1 )
DEFAULT_SRC_INSTALL_EXTRA_DOCS=( greylist2.conf )

milter-greylist_src_prepare() {
    edo sed -i -e 's:\(${DESTDIR}/var\)\(/milter-greylist\):\1/lib\2:' Makefile.in

    default
}

milter-greylist_src_install() {
    default

    install_systemd_files

    keepdir /var/lib/milter-greylist
}

