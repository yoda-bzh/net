# Copyright 2013 Pierre Lejeune <superheron@gmail.com>
# Copyright 2014-2024 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require cmake
require ffmpeg [ options="[vaapi?]" with_opt=true ]
require option-renames [ renames=[ 'va vaapi' ] ]
require freedesktop-desktop

SUMMARY="FreeRDP: A Remote Desktop Protocol implementation"
DESCRIPTION="
FreeRDP is a free implementation of the Remote Desktop Protocol (RDP), released under the Apache license.
"
HOMEPAGE+=" https://www.freerdp.com"
DOWNLOADS="https://pub.freerdp.com/releases/${PNV,,}.tar.gz"

LICENCES="Apache-2.0"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    aac
    alsa
    cups
    fdk-aac [[ description = [ Support for using the Frauenhofer AAC Codec for sound and microphone redirection ] ]]
    kerberos
    mp3
    pcsc [[ description = [ Support for smartcard authentification via pcsc-lite ] ]]
    pulseaudio
    sdl
    server [[ description = [ Build the FreeRDP server ] ]]
    soxr [[ description = [ Support for audio resampling via libsoxr ] ]]
    systemd
    vaapi [[
        description = [ Enable support for decoding video using the Video Acceleration API ]
        requires = [ ffmpeg ]
    ]]
    wayland
    kerberos? ( ( providers: heimdal krb5 ) [[ number-selected = exactly-one ]] )
    ( libc: musl )
    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        dev-libs/icu:=
        dev-libs/json-c:=
        dev-libs/libusb:1
        media-libs/libpng:=
        sys-libs/pam
        sys-libs/zlib
        x11-libs/cairo
        x11-libs/libX11
        x11-libs/libXcursor
        x11-libs/libXext
        x11-libs/libXfixes
        x11-libs/libXi
        x11-libs/libXinerama
        x11-libs/libXrandr
        x11-libs/libXrender
        x11-libs/libXtst
        x11-libs/libXv
        x11-libs/libxkbfile
        aac? (
            media-libs/faac
            media-libs/faad2
        )
        alsa? ( sys-sound/alsa-lib )
        cups? ( net-print/cups )
        fdk-aac? ( media-libs/fdk-aac )
        kerberos? (
            providers:heimdal? ( app-crypt/heimdal )
            providers:krb5? ( app-crypt/krb5 )
        )
        libc:musl? ( dev-libs/libexecinfo )
        mp3? ( media-sound/lame )
        pcsc? ( sys-apps/pcsc-lite )
        providers:ijg-jpeg? ( media-libs/jpeg:= )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl:= )
        pulseaudio? ( media-sound/pulseaudio )
        sdl? (
            media-libs/SDL:2
            media-libs/SDL_ttf:2
        )
        server? ( x11-libs/libXdamage )
        soxr? ( media-libs/soxr )
        systemd? ( sys-apps/systemd )
        wayland? (
            sys-libs/wayland
            x11-libs/libxkbcommon
        )
"

CMAKE_SOURCE=${WORKBASE}/${PNV,,}

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DCMAKE_BUILD_TYPE:STRING=Release
    -DCMAKE_FIND_ROOT_PATH:PATH="$(ffmpeg_alternatives_prefix);/usr/$(exhost --target)"
    -DBUILD_FUZZERS:BOOL=FALSE
    -DBUILD_SHARED_LIBS:BOOL=TRUE
    -DBUILD_TESTING_INTERNAL:BOOL=FALSE
    -DENABLE_WARNING_ERROR:BOOL=FALSE
    -DFREERDP_UNIFIED_BUILD:BOOL=TRUE
    -DRDTK_FORCE_STATIC_BUILD:BOOL=FALSE
    -DSDL_USE_COMPILED_RESOURCES:BOOL=TRUE
    -DUSE_EXECINFO:BOOL=TRUE
    -DUWAC_FORCE_STATIC_BUILD:BOOL=FALSE
    -DWINPR_UTILS_IMAGE_JPEG:BOOL=TRUE
    -DWINPR_UTILS_IMAGE_PNG:BOOL=TRUE
    -DWINPR_UTILS_IMAGE_WEBP:BOOL=FALSE
    -DWITH_AAD:BOOL=TRUE
    -DWITH_ABSOLUTE_PLUGIN_LOAD_PATHS:BOOL=FALSE
    -DWITH_BINARY_VERSIONING:BOOL=FALSE
    -DWITH_CAIRO:BOOL=TRUE
    -DWITH_CCACHE:BOOL=FALSE
    -DWITH_CHANNELS:BOOL=TRUE
    -DWITH_CJSON_REQUIRED:BOOL=FALSE
    -DWITH_CLANG_FORMAT:BOOL=FALSE
    -DWITH_CLIENT:BOOL=TRUE
    -DWITH_CLIENT_CHANNELS:BOOL=TRUE
    -DWITH_CLIENT_COMMON:BOOL=TRUE
    -DWITH_CLIENT_INTERFACE:BOOL=FALSE
    -DWITH_CLIENT_SDL_VERSIONED:BOOL=FALSE
    -DWITH_CLIENT_SDL3:BOOL=FALSE
    -DWITH_DEBUG_ALL:BOOL=FALSE
    -DWITH_DOCUMENTATION:BOOL=FALSE
    -DWITH_DSP_EXPERIMENTAL:BOOL=FALSE
    -DWITH_FREERDS:BOOL=FALSE
    -DWITH_FUSE:BOOL=FALSE
    -DWITH_GPROF:BOOL=FALSE
    -DWITH_GSM:BOOL=FALSE
    -DWITH_ICU:BOOL=TRUE
    -DWITH_INSTALL_CLIENT_DESKTOP_FILES:BOOL=TRUE
    -DWITH_INTERNAL_MD4:BOOL=TRUE
    -DWITH_INTERNAL_MD5:BOOL=TRUE
    -DWITH_JPEG:BOOL=TRUE
    -DWITH_JSON_REQUIRED:BOOL=TRUE
    -DWITH_JSON_DISABLED:BOOL=FALSE
    -DWITH_KEYBOARD_LAYOUT_FROM_FILE:BOOL=TRUE
    -DWITH_LODEPNG:BOOL=FALSE
    -DWITH_MANPAGES:BOOL=TRUE
    -DWITH_MBEDTLS:BOOL=FALSE
    -DWITH_MEDIACODEC:BOOL=FALSE
    -DWITH_NATIVE_SSPI:BOOL=FALSE
    -DWITH_OPAQUE_SETTINGS:BOOL=FALSE
    -DWITH_OPENCL:BOOL=FALSE
    -DWITH_OPENH264:BOOL=FALSE
    -DWITH_OPENH264_LOADING:BOOL=FALSE
    -DWITH_OPENSLES:BOOL=FALSE
    -DWITH_OPENSSL:BOOL=TRUE
    -DWITH_OSS:BOOL=TRUE
    -DWITH_PKCS11:BOOL=FALSE
    -DWITH_PLATFORM_SERVER:BOOL=FALSE
    -DWITH_POLL:BOOL=TRUE
    -DWITH_PROFILER:BOOL=FALSE
    -DWITH_PROXY_EMULATE_SMARTCARD:BOOL=FALSE
    -DWITH_RESOURCE_VERSIONING:BOOL=FALSE
    -DWITH_SAMPLE:BOOL=FALSE
    -DWITH_SANITIZE_ADDRESS:BOOL=FALSE
    -DWITH_SANITIZE_MEMORY:BOOL=FALSE
    -DWITH_SANITIZE_THREAD:BOOL=FALSE
    -DWITH_SDL_IMAGE_DIALOGS:BOOL=FALSE
    -DWITH_SMARTCARD_EMULATE:BOOL=FALSE
    -DWITH_SMARTCARD_INSPECT:BOOL=FALSE
    -DWITH_SWSCALE:BOOL=FALSE
    -DWITH_THIRD_PARTY:BOOL=FALSE
    -DWITH_TIMEZONE_COMPILED:BOOL=FALSE
    -DWITH_TIMEZONE_FROM_FILE:BOOL=TRUE
    -DWITH_TIMEZONE_ICU:BOOL=FALSE
    -DWITH_TIMEZONE_UPDATER:BOOL=FALSE
    -DWITH_UNICODE_BUILTIN:BOOL=FALSE
    -DWITH_URIPARSER:BOOL=FALSE
    -DWITH_VALGRIND_MEMCHECK:BOOL=FALSE
    -DWITH_VERBOSE_WINPR_ASSERT:BOOL=FALSE
    -DWITH_WEBVIEW:BOOL=FALSE
    -DWITH_WEBVIEW_QT:BOOL=FALSE
    -DWITH_WIN_CONSOLE:BOOL=FALSE
    -DWITH_WINPR_DEPRECATED:BOOL=FALSE
    -DWITH_WINPR_TOOLS:BOOL=TRUE
    -DWITH_X11:BOOL=TRUE
    -DWITH_XCURSOR:BOOL=TRUE
    -DWITH_XEXT:BOOL=TRUE
    -DWITH_XFIXES:BOOL=TRUE
    -DWITH_XI:BOOL=TRUE
    -DWITH_XINERAMA:BOOL=TRUE
    -DWITH_XRANDR:BOOL=TRUE
    -DWITH_XRENDER:BOOL=TRUE
    -DWITH_XV:BOOL=TRUE
)
CMAKE_SRC_CONFIGURE_OPTION_WITHS=(
    'aac FAAC'
    'aac FAAD2'
    'alsa ALSA'
    'cups CUPS'
    'fdk-aac FDK_AAC'
    'ffmpeg DSP_FFMPEG'
    'ffmpeg FFMPEG'
    'ffmpeg VIDEO_FFMPEG'
    'kerberos KRB5'
    'mp3 LAME'
    'pcsc PCSC'
    'pcsc SMARTCARD_PCSC'
    'providers:libressl LIBRESSL'
    'pulseaudio PULSE'
    'sdl CLIENT_SDL'
    'sdl CLIENT_SDL2'
    'server PROXY'
    'server PROXY_APP'
    'server PROXY_MODULES'
    'server RDTK'
    'server SERVER'
    'server SERVER_CHANNELS'
    'server SERVER_INTERFACE'
    'server SHADOW'
    'soxr SOXR'
    'systemd SYSTEMD'
    'vaapi VAAPI'
    'wayland WAYLAND'
)
CMAKE_SRC_CONFIGURE_TESTS=(
    '-DBUILD_TESTING:BOOL=TRUE -DBUILD_TESTING:BOOL=FALSE'
)

src_test() {
    esandbox allow_net "unix:${TEMP%/}/.pipe/*"
    cmake_src_test
    esandbox disallow_net "unix:${TEMP%/}/.pipe/*"
}

src_install() {
    cmake_src_install

    # remove empty directories
    edo find "${IMAGE}" -type d -empty -delete
}

