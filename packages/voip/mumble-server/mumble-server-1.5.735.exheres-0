# Copyright 2010-2011 Johannes Nixdorf <mixi@user-helfen-usern.de>
# Copyright 2012-2013 Lasse Brun <bruners@gmail.com>
# Copyright 2013-2024 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'mumble-1.2.2.ebuild' from Gentoo, which is:
#     Copyright 1999-2010 Gentoo Foundation

MY_PN=${PN/-server}

require github [ user=mumble-voip release=v${PV} suffix=tar.gz ] \
    cmake \
    systemd-service

SUMMARY="Mumble is an open source, low-latency, high quality voice chat software"
DESCRIPTION="
Mumble is a voice chat application for groups. While it can be used for any kind of activity, it is
primarily intended for gaming. It can be compared to programs like Ventrilo or TeamSpeak. People
tend to simplify things, so when they talk about Mumble they either talk about \"Mumble\" the
client application or about \"Mumble & Mumble Server\" the whole voice chat application suite.
"
HOMEPAGE+=" https://mumble.info"

LICENCES="BSD-3"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    avahi
    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        dev-lang/python:*[>=3]
        dev-libs/boost[>=1.41.0]
        virtual/pkg-config
    build+run:
        group/${PN}
        user/${PN}
        dev-libs/protobuf:=
        sys-libs/libcap
        x11-libs/qtbase:5[?gui][sql]
        avahi? ( net-dns/avahi[dns_sd] )
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl:= )
        !media-sound/mumble [[
            description = [ voip/murmur was previously part of media-sound/mumble ]
            resolution = uninstall-blocked-before
        ]]
"

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DCMAKE_CXX_STANDARD:STRING=17
    -DBUILD_NUMBER:STRING=$(ever range 3)
    -DMUMBLE_INSTALL_SYSCONFDIR:PATH=/etc/${PN}
    -DMUMBLE_INSTALL_SERVICEFILEDIR:PATH=${SYSTEMDSYSTEMUNITDIR}
    -DMUMBLE_INSTALL_TMPFILESDIR:PATH=${SYSTEMDTMPFILESDIR}
    -Dbenchmarks:BOOL=FALSE
    -Dclient:BOOL=FALSE
    -Dice:BOOL=FALSE
    -Dlto:BOOL=FALSE
    -Doptimize:BOOL=FALSE
    -Dserver:BOOL=TRUE
    -Dsymbols:BOOL=FALSE
    -Dtests:BOOL=FALSE
    -Dtracy:BOOL=FALSE
    -Dwarnings-as-errors:BOOL=FALSE
)

CMAKE_SRC_CONFIGURE_OPTIONS=(
    'avahi zeroconf'
)

src_prepare() {
    cmake_src_prepare

    # database/logs
    edo sed \
        -e 's:database=:database=/var/lib/mumble-server/mumble-server.sqlite:g' \
        -e 's:mumble-server.log:/var/log/mumble-server/mumble-server.log:g' \
        -i auxiliary_files/mumble-server.ini

    # user/group
    edo sed \
        -e 's:_mumble-server:mumble-server:g' \
        -i auxiliary_files/config_files/mumble-server.{service,tmpfiles}.in
}

src_install() {
    cmake_src_install

    # remove sysusers file
    edo rm -rf "${IMAGE}"/usr/$(exhost --target)/etc

    edo chown root:mumble-server "${IMAGE}"/etc/mumble-server/mumble-server.ini
    edo chmod 0640 "${IMAGE}"/etc/mumble-server/mumble-server.ini

    keepdir /var/{lib,log}/mumble-server
    edo chown mumble-server:mumble-server "${IMAGE}"/var/{lib,log}/mumble-server
}

