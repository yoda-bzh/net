# Copyright 2023 Morgane “Sardem FF7” Glidic <sardemff7@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=cyrusimap release=${PNV} suffix=tar.gz ]

SUMMARY="An email, contacts and calendar server"
HOMEPAGE="https://www.cyrusimap.org/"

LICENCES="cyrus-sasl"
SLOT="0"
MYOPTIONS="
    calendar [[
        description = [ CalDAV and CardDAV support ]
        requires = [ sqlite ]
    ]]
    postgresql
    sieve [[ requires = [ sqlite ] ]]
    sqlite
"

DEPENDENCIES="
    build:
        sieve? (
            sys-devel/bison
            sys-devel/flex
        )
    build+run:
        dev-lang/perl:=
        dev-libs/icu:=
        dev-libs/jansson
        dev-libs/libbsd
        dev-libs/openssl:=
        dev-libs/pcre
        net-libs/cyrus-sasl
        sys-apps/util-linux [[ note = [ For libuuid ] ]]
        sys-fs/e2fsprogs [[ note = [ For libcom_err ] ]]
        sys-libs/libcap
        sys-libs/zlib
        user/cyrus
        user/mail
        group/mail
        calendar? (
            dev-libs/libxml2:2.0
            office-libs/libical:3[>=3.0.10]
        )
        postgresql? ( dev-db/postgresql-client )
        sqlite? ( dev-db/sqlite:3 )
    test:
        dev-util/cunit
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --bindir=/usr/$(exhost --target)/libexec/${PN}
    --sbindir=/usr/$(exhost --target)/libexec/${PN}
    --libexecdir=/usr/$(exhost --target)/libexec/${PN}

    --enable-idled
    --enable-pcre
    --enable-squat
    --with-extraident=Exherbo
    --with-libcap
    --with-openssl
    --with-pidfile=/run/cyrus-master.pid
    --with-zlib

    --disable-afs
    --disable-backup
    --disable-murder
    --disable-nntp
    --disable-objectstore
    --disable-oldsievename
    --disable-replication
    --disable-srs
    --disable-xapian
    --without-brotli
    --without-caringo
    --without-chardet
    --without-cld2
    --without-guesstz
    --without-ldap
    --without-mysql
    --without-nghttp2
    --without-openio
    --without-wslay
    --without-zephyr
    --without-zeroskip
    --without-zstd
)
DEFAULT_SRC_CONFIGURE_OPTION_WITHS=(
    'postgresql pgsql'
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    'calendar calalarmd'
    'calendar http'
)
DEFAULT_SRC_CONFIGURE_TESTS=(
    '--enable-unit-tests --disable-unit-tests'
)


