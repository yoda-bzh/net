# Copyright 2018-2025 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require cargo [ rust_minimum_version=1.66.0 ] \
    github [ user=element-hq release=v${PV} pnv=matrix_${PNV} suffix=tar.gz ] \
    setup-py [ import=setuptools has_bin=true multibuild=false blacklist="2 3.8" test=pytest ] \
    systemd-service

SUMMARY="Matrix reference homeserver"
DESCRIPTION="
Synapse is the reference python/twisted Matrix homeserver implementation.
"
HOMEPAGE+=" https://matrix.org"

LICENCES="AGPL-3"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

# Missing test dependencies
RESTRICT="test"

# FIXME: add opentracing and sentry suggestions
DEPENDENCIES="
    build+run:
        group/${PN}
        user/${PN}
        dev-python/attrs[>=19.2.0][python_abis:*(-)?]
        dev-python/bcrypt[>=3.1.7][python_abis:*(-)?]
        dev-python/bleach[>=1.4.3][python_abis:*(-)?]
        dev-python/canonicaljson[>=2.0.0&<3.0.0][python_abis:*(-)?]
        dev-python/cryptography[>=3.4.7][python_abis:*(-)?]
        dev-python/ijson[>=3.1.4][python_abis:*(-)?]
        dev-python/immutabledict[>=2.0][python_abis:*(-)?]
        dev-python/Jinja2[>=3.0][python_abis:*(-)?]
        dev-python/jsonschema[>=3.0.0][python_abis:*(-)?]
        dev-python/matrix-common[>=1.3.0&<2.0.0][python_abis:*(-)?]
        dev-python/msgpack[>=0.5.2][python_abis:*(-)?]
        dev-python/netaddr[>=0.7.18][python_abis:*(-)?]
        dev-python/packaging[>=20.0][python_abis:*(-)?]
        dev-python/phonenumbers[>=8.2.0][python_abis:*(-)?]
        dev-python/Pillow[>=10.0.1][python_abis:*(-)?]
        dev-python/prometheus_client[>=0.4.0][python_abis:*(-)?]
        dev-python/pyasn1[>=0.1.9][python_abis:*(-)?]
        dev-python/pyasn1-modules[>=0.0.7][python_abis:*(-)?]
        dev-python/pydantic[>=1.7.4&<3][python_abis:*(-)?]
        dev-python/pymacaroons[>=0.13.0][python_abis:*(-)?]
        dev-python/pyopenssl[>=16.0.0][python_abis:*(-)?]
        dev-python/python-multipart[>=0.0.9][python_abis:*(-)?]
        dev-python/PyYAML[>=5.3][python_abis:*(-)?]
        dev-python/service_identity[>=18.1.0][python_abis:*(-)?]
        dev-python/setuptools-rust[>=1.3][python_abis:*(-)?]
        dev-python/signedjson[>=1.1.0&<2.0.0][python_abis:*(-)?]
        dev-python/sortedcontainers[>=1.5.2][python_abis:*(-)?]
        dev-python/treq[>=15.1][python_abis:*(-)?]
        dev-python/typing-extensions[>=4.1][python_abis:*(-)?]
        dev-python/unpaddedbase64[>=2.1.0][python_abis:*(-)?]
        net-twisted/Twisted[>=18.9.0][python_abis:*(-)?]
    suggestion:
        dev-libs/xmlsec [[
            description = [ Required for SAML 2.0 single sign-on support ]
        ]]
        dev-python/Authlib[>=0.15.1][python_abis:*(-)?] [[
            description = [ Adds OpenID Connect and JWT authentification support to Synapse ]
        ]]
        dev-python/hiredis[python_abis:*(-)?] [[
            description = [ Required to run the replication protocol over Redis faster ]
        ]]
        dev-python/lxml[>=4.5.2][python_abis:*(-)?] [[
            description = [ Required for URL preview support ]
        ]]
        dev-python/matrix-synapse-ldap3[>=0.1][python_abis:*(-)?] [[
            description = [ Adds LDAP authentification support to Synapse ]
        ]]
        dev-python/matrix-synapse-rest-password-provider[python_abis:*(-)?] [[
            description = [ Adds REST authentication support to Synapse ]
        ]]
        dev-python/psycopg2[>=2.8][python_abis:*(-)?] [[
            description = [ Required for PostgreSQL support ]
        ]]
        dev-python/PyICU[>=2.10.2][python_abis:*(-)?] [[
            description = [ Required for improved user search with better Unicode support ]
        ]]
        dev-python/Pympler[python_abis:*(-)?] [[
            description = [ Required for the caches.track_memory_usage config option ]
        ]]
        dev-python/pysaml2[>=4.5.0][python_abis:*(-)?] [[
            description = [ Required for SAML 2.0 single sign-on support ]
        ]]
        dev-python/txredisapi[>=1.4.7][python_abis:*(-)?] [[
            description = [ Required to run the replication protocol over Redis ]
        ]]
        sys-apps/systemd[>=231][python_abis:*(-)?] [[
            description = [ Required for logging to the systemd journal ]
        ]]
"

prepare_one_multibuild() {
    setup-py_prepare_one_multibuild

    ecargo_fetch
}

src_configure() {
    setup-py_src_configure

    cargo_src_configure
}

src_install() {
    setup-py_src_install

    keepdir /etc/synapse
    edo chown synapse:synapse "${IMAGE}"/etc/synapse
    edo chmod 0750 "${IMAGE}"/etc/synapse

    keepdir /var/lib/synapse
    edo chown synapse:synapse "${IMAGE}"/var/lib/synapse

    install_systemd_files

    # respect Python ABI
    edo sed \
        -e "s:/usr/bin/python:${PYTHON}:" \
        -i "${IMAGE}"/${SYSTEMDSYSTEMUNITDIR}/${PN}.service
}

pkg_postinst() {
    elog "Generate a default configuration file:"
    elog "cd /var/lib/synapse"
    elog "${PYTHON} -m synapse.app.homeserver \\"
    elog "    --server-name my.domain.name \\"
    elog "    --config-path /etc/synapse/homeserver.yaml \\"
    elog "    --generate-config \\"
    elog "    --report-stats=no"
}

