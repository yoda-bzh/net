# Copyright 2021 Bernd Steinhauser <berniyh@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=open-iscsi tag=v${PV} ] meson systemd-service

export_exlib_phases src_install

SUMMARY="(Partial) Implementation of iSNS"
DESCRIPTION="
This is a partial implementation of iSNS, according to RFC4171.
The implementation is still somewhat incomplete, but I'm releasing
it for your reading pleasure.
"

LICENCES="GPL-2"
SLOT="0"
MYOPTIONS="
    slp
    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl:= )
        slp? ( net-libs/openslp )
"

MESON_SRC_CONFIGURE_PARAMS=(
    -Drundir=/run
    -Dsecurity=enabled
    -Dshared_version=true
    -Dsystemddir=${SYSTEMDSYSTEMUNITDIR%/system}
)

MESON_SRC_CONFIGURE_OPTION_FEATURES=(
    slp
)


open-isns_src_install() {
    meson_src_install

    keepdir /var/lib/isns
}

